# WARNING:
# This Dockerfile builds a TESTING image for the frontend

FROM node:latest

RUN mkdir /frontend
COPY . /frontend
WORKDIR /frontend

RUN npm install -g serve
RUN echo "VUE_APP_API_URL='localhost:8000'" > .env
RUN echo "VUE_APP_WEBSOCKET_URL='localhost:4001'" >> .env
RUN npm ci
RUN npm run build_test

CMD serve -s dist