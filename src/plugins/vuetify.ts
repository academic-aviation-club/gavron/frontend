import Vue from 'vue';
import Vuetify from 'vuetify/lib';
// import colors from 'vuetify/lib/util/colors';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                primary: '#ac526c',
                secondary: '#bccee6',
                accent: '#ac526c',
            },
        },
        options: {
            customProperties: true,
        },
    },
});
