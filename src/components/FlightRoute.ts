import { LatLng } from "leaflet";

type Meters = number;
type KilometersPerHour = number;

export default class FlightRoute {
    segments = [] as FlightRouteSegment[];

    name = "New route";

    encodeRoute(): string {
        const obj = { name: this.name, segments: this.segments };
        return JSON.stringify(obj);
    }

    decodeRoute(route_json: string) {
        const decode_obj: any = JSON.parse(route_json);
        this.segments = decode_obj.segments;
        this.name = decode_obj.name;
    }
}

export class FlightRouteSegment {
    constructor(
        public altitude: Meters,
        public end_position: LatLng,
        public ground_speed: KilometersPerHour,
        public camera_on: boolean,
        public look_at: LatLng,
    ) { }
}
