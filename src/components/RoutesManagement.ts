import FlightRoute from "@/components/FlightRoute";
import axios from "axios";
import API_URL from "@/api/api_url";

const ROUTE_FOLDER = "route";

export default class RouteData {
    id: number;

    route: FlightRoute;

    constructor(id: number, route: FlightRoute) {
        this.id = id;
        this.route = route;
    }
}

export async function loadRoutesFromServer() {
    const routes = await axios.get(`${API_URL}/${ROUTE_FOLDER}/`);
    const route_data = routes.data;

    const route_data_list = [] as RouteData[];
    for (const r of route_data) {
        const route = new FlightRoute();
        route.decodeRoute(r.drone_route);
        route_data_list.push(new RouteData(r.id, route));
    }
    return route_data_list;
}

export async function saveRouteOnServer(route: FlightRoute) {
    const encoded_route = route.encodeRoute();

    await axios.post(`${API_URL}/${ROUTE_FOLDER}/`, {
      drone_route: encoded_route,
    });
}

export async function modifyRouteOnServer(route: FlightRoute, route_id: number) {
    await axios.delete(`${API_URL}/${ROUTE_FOLDER}/${route_id}`);
    saveRouteOnServer(route);
}

export async function deleteRouteOnServer(route_id: number) {
    await axios.delete(`${API_URL}/${ROUTE_FOLDER}/${route_id}`);
}
