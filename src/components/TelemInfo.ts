import { GavronTelem } from '@/telemetry_definitions/generated/GavronTelem';

export default class TelemInfo {
    drone_pos!: GavronTelem.Position;

    drone_id!: number;

    flight_id!: number;
}
