import L from "leaflet";

class LaunchPoint {
    constructor(
        public name: string,
        public lat: number,
        public lng: number
    ) { }
}

const launch_points = [
    new LaunchPoint('Legnica', 51.18268, 16.17713),
    new LaunchPoint('Morskie Oko', 51.124742, 17.087180),
    new LaunchPoint('Święta Katarzyna', 51.017855, 17.102918),
    new LaunchPoint('AKL HQ', 51.11399, 17.06424),
];

export default function setup_launch_points(map: L.Map) {
    console.log("Setting up default launch sites");
    console.log(launch_points);

    const DEFAULT_TOOLTIP_SETTINGS = {
        permanent: true,
        interactive: true
    };

    const launch_tooltips_list = [] as L.Tooltip[];

    for (const launch_point of launch_points) {
        const launch_tooltip = new L.Tooltip(DEFAULT_TOOLTIP_SETTINGS);

        launch_tooltip.setLatLng([launch_point.lat, launch_point.lng])
        .setContent(launch_point.name).addTo(map);

        // adds event listener to DOM object
        const el = launch_tooltip.getElement();
        el!.addEventListener('click', (event: Event) => {
            // disable event propagation so the marker isn't placed on tooltip click
            event.stopPropagation();
            map.setView([launch_point.lat, launch_point.lng], 16);
        });
        el!.style.pointerEvents = 'auto';

        launch_tooltips_list.push(launch_tooltip);
    }
    // TODO: try to fix the issue with tooltips default control state as false

    const launch_tooltips = L.layerGroup(launch_tooltips_list);

    const mapOverlay = {
        "Launch points": launch_tooltips
    };

    // @ts-ignore
    L.control.layers(null, mapOverlay).addTo(map);
}
