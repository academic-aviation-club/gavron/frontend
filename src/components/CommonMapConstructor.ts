import L from "leaflet";
import setup_default_launch_points from "@/components/DefaultLaunchSitesWayPoints";

export default function common_map_constructor(map_id: string) {
    const DEFAULT_LAT = 51.18268;
    const DEFAULT_LNG = 16.17713;
    const DEFAULT_ZOOM = 16;

    const map = L.map(map_id).setView([DEFAULT_LAT, DEFAULT_LNG], DEFAULT_ZOOM);
    setup_default_launch_points(map);

    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(map);

    return map;
}
