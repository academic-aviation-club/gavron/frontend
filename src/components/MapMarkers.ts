import "leaflet/dist/leaflet.css";
import L from "leaflet";
import "leaflet-rotatedmarker";

const PLANNER_START_NODE_ICON_PATH = require("@/assets/start_node_icon.png");
const DRONE_ICON_PATH = require("@/assets/drone_icon.png");
const DRONE_DROP_ICON_PATH = require("@/assets/drone_drop_icon.png");
const PREDICTED_DROP_ICON_PATH = require("@/assets/predicted_drop_icon.png");
const ACTUAL_DROP_ICON_PATH = require("@/assets/actual_drop_icon.png");

const PLANNER_NODE_ICON_PATH = "https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678111-map-marker-512.png";

export class DroneMarker extends L.Marker {
    static drone_icon = L.icon({
        iconUrl: DRONE_ICON_PATH,

        iconSize: [50, 50],
        iconAnchor: [25, 25],
    });

    constructor(latLng: L.LatLng) {
        super(latLng, {
            icon: DroneMarker.drone_icon,
            // @ts-ignore
            rotationOrigin: "center",
        });
    }
}

export class DroneDropMarker extends L.Marker {
    static drone_icon = L.icon({
        iconUrl: DRONE_DROP_ICON_PATH,

        iconSize: [50, 50],
        iconAnchor: [25, 25],
    });

    constructor(latLng: L.LatLng) {
        super(latLng, {
            icon: DroneDropMarker.drone_icon,
            // @ts-ignore
            rotationOrigin: "center",
        });
    }
}

export class DropMarker extends L.Marker {
    static predicted_drop_icon = L.icon({
        iconUrl: PREDICTED_DROP_ICON_PATH,

        iconSize: [25, 25],
        iconAnchor: [12.5, 12.5],
    });

    static actual_drop_icon = L.icon({
        iconUrl: ACTUAL_DROP_ICON_PATH,

        iconSize: [25, 25],
        iconAnchor: [12.5, 12.5],
    });

    constructor(latLng: L.LatLng) {
        super(latLng, {
            opacity: 1,
        });
        this.setIcon(DropMarker.predicted_drop_icon);
    }
}

export class RouteNode extends L.Marker {
    static route_node_icon = L.icon({
        iconUrl: PLANNER_NODE_ICON_PATH,

        iconSize: [50, 50],
        iconAnchor: [25, 48],
    });

    static route_start_node_icon = L.icon({
        iconUrl: PLANNER_START_NODE_ICON_PATH,

        iconSize: [50, 50],
        iconAnchor: [25, 25],
    });

    constructor(latLng: L.LatLng) {
        super(latLng, {
            draggable: true,
        });

        this.setIcon(RouteNode.route_node_icon);
    }
}
