/*
  BUILD_TYPE
*/

public_paths = {
  production: '/',
  test: '/',
  development: '/',
}

module.exports = {
  transpileDependencies: [
    'vuetify',
  ],
  publicPath: public_paths[process.env.NODE_ENV],
};
