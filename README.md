# Gavron Front

Hosted [here](http://vps-e584bd76.vps.ovh.net:5000/)

*Note*: use the `http` version, since the unsafe in-development APIs won't work with `https`.
If your browser automatically redirects to `https`, try in incognito.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
