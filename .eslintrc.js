module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
    '@vue/typescript/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'quotes': 'off',
    'vue/no-unused-components': 'warn',
    'indent': 'off',
    '@typescript-eslint/no-explicit-any': 'warn',
    'class-methods-use-this': 'warn',
    'max-classes-per-file': 'off',
    'no-useless-constructor': 'off',
    '@typescript-eslint/camelcase': 'off',
    "no-restricted-syntax": 'warn',
    "comma-dangle": 'off',
    "@typescript-eslint/ban-ts-ignore": 'warn',
    "import/extensions": 'off',
    "@typescript-eslint/no-var-requires": 'off',
  },
};
